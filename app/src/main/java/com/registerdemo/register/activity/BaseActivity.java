package com.registerdemo.register.activity;


import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.registerdemo.R;
import java.util.regex.Pattern;


/**
 * Created by Mobile on 1/2/2018.
 */

public class BaseActivity extends AppCompatActivity {
    private AlertDialog dialogProgress;
    public void showToast(String s){
        if(s.length()<15){
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        }
    }

    public  boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }
    public  void baseShowProgressDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.view_loading_customdialog, null);
        TextView textViewMessage = (TextView) view.findViewById(R.id.textViewMessage);
        textViewMessage.setText("Loading...Please Wait");
        alertDialogBuilder.setView(view);
        dialogProgress = alertDialogBuilder.create();
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogProgress.show();
    }
    public  void baseHideProgressDialog() {
        if(dialogProgress!=null){
            dialogProgress.dismiss();
        }
    }
}
