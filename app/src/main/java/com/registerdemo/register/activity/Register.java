package com.registerdemo.register.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.registerdemo.R;
import com.registerdemo.home.activity.HomeActivity;
import com.registerdemo.register.model.RegisterProp;
import com.registerdemo.register.presenter.RegisterPresenter;
import com.registerdemo.register.view.IRegisterView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Register extends BaseActivity implements IRegisterView {
    @BindView(R.id.etFirstName)EditText etFirstName;
    @BindView(R.id.etLastName)EditText  etLastName;
    @BindView(R.id.etEmail)EditText etEmail;
    @BindView(R.id.etPassword)EditText etPassword;
    @BindView(R.id.etConfirmPassword)EditText etConfirmPassword;
    @BindView(R.id.etPhoneNumber)EditText etPhoneNumber;


    RegisterPresenter registerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        registerPresenter=new RegisterPresenter(this,this);
    }

    @Override
    public void onCompleteRegistration(RegisterProp registerProp) {
        Intent intent=new Intent(this,HomeActivity.class);
        intent.putExtra("register",registerProp);
        startActivity(intent);
        finish();
    }
    @OnClick(R.id.btnRegister)
    public void register(){
        registerPresenter.requestRegister(etFirstName.getText().toString(),etLastName.getText().toString(),etEmail.getText().toString(),etPassword.getText().toString(),etConfirmPassword.getText().toString(),etPhoneNumber.getText().toString());
    }

}
