package com.registerdemo.register.view;


import com.registerdemo.register.model.RegisterProp;

/**
 * Created by Mobile on 6/4/2018.
 */

public interface IRegisterView {
    void onCompleteRegistration(RegisterProp registerProp);
}
