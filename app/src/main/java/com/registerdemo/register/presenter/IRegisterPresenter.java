package com.registerdemo.register.presenter;


import com.registerdemo.register.model.RegisterProp;

/**
 * Created by Mobile on 6/4/2018.
 */

public interface IRegisterPresenter {
    void requestRegister(String first_name, String last_name, String email, String password, String confirmPassword, String number);
    void onResultRegistration(RegisterProp registerProp);
}
