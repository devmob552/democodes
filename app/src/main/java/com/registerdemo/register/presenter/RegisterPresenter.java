package com.registerdemo.register.presenter;

import com.registerdemo.register.activity.BaseActivity;
import com.registerdemo.register.model.RegisterProp;
import com.registerdemo.register.view.IRegisterView;



/**
 * Created by Mobile on 6/4/2018.
 */

public class RegisterPresenter implements IRegisterPresenter {
    BaseActivity baseActivity;
    IRegisterView iRegisterView;

    public RegisterPresenter(BaseActivity baseActivity, IRegisterView iRegisterView) {
        this.baseActivity = baseActivity;
        this.iRegisterView = iRegisterView;
    }

    @Override
    public void requestRegister( String first_name, String last_name, String email, String password, String confirmPassword, String number) {
        if(first_name.trim().length()==0){
            baseActivity.showToast("Please Enter First Name");
        }else if(last_name.trim().length()==0){
            baseActivity.showToast("Please Enter Last Name");
        }else if(email.trim().length()==0){
            baseActivity.showToast("Please Enter Email");
        }else if(!baseActivity.isValidEmaillId(email)){
            baseActivity.showToast("Please Enter Valid Email");
        }else if(password.trim().length()==0){
            baseActivity.showToast("Please Enter Password");
        }else if(!password.equals(confirmPassword)){
            baseActivity.showToast("Please Confirm Password");
        }else if(number.trim().length()==0){
            baseActivity.showToast("Please Enter Phone Number");
        }
        else {
            baseActivity.baseShowProgressDialog();
            RegisterProp registerProp=new RegisterProp();
            registerProp.setFirstName(first_name);
            registerProp.setLastName(last_name);
            registerProp.setEmail(email);
            registerProp.setPassword(password);
            registerProp.setPhone(number);
            onResultRegistration(registerProp);
        }
    }

    @Override
    public void onResultRegistration(RegisterProp registerProp) {
        baseActivity.baseHideProgressDialog();
        if(registerProp!=null){
            baseActivity.showToast("Registerd Successfully");
            iRegisterView.onCompleteRegistration(registerProp);
        }else {
            baseActivity.showToast("Unable To Register Please Try Again");
        }
    }
}
