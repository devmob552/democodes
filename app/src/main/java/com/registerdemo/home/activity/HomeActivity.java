package com.registerdemo.home.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.registerdemo.R;
import com.registerdemo.register.model.RegisterProp;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
    RegisterProp registerProp;
    @BindView(R.id.tvFirstName)
    TextView tvFirstName;
    @BindView(R.id.tvLastName)
    TextView tvLastName;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.tvPassword)
    TextView tvPassword;
    @BindView(R.id.tvPhone)
    TextView tvPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        registerProp = (RegisterProp) getIntent().getSerializableExtra("register");
        tvFirstName.setText(registerProp.getFirstName());
        tvLastName.setText(registerProp.getLastName());
        tvEmail.setText(registerProp.getEmail());
        tvPassword.setText(registerProp.getPassword());
        tvPhone.setText(registerProp.getPhone());
    }
}
